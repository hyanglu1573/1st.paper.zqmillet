%!TEX root = ./Main.tex

\usepackage{amsmath}
\usepackage{mathrsfs}
\usepackage{mathtools}
\usepackage{commath}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{cases}
\newcounter{subeqn} 
\renewcommand{\thesubeqn}{\theequation\alph{subeqn}}%
\newcommand{\subeqn}{%
  \refstepcounter{subeqn}% Step subequation number
  \tag{\thesubeqn}% Label equation
}

\usepackage[american]{babel}
\usepackage{microtype}

\usepackage{tabu, longtable}
\usepackage{multicol}
\usepackage{colortbl}
\usepackage{rotating}
\usepackage{enumitem}
\usepackage{multirow}
\usepackage{color}
\usepackage{tikz, pgfplots, tkz-euclide}
\usetikzlibrary{calc}
\usetikzlibrary{shadows}
\usetikzlibrary{positioning}
\usetikzlibrary{circuits.ee.IEC}
\usetikzlibrary{decorations.pathmorphing}
\usetikzlibrary{shapes.symbols}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{intersections}
\usetikzlibrary{spy}
\usepgfplotslibrary{fillbetween}
\usetkzobj{all}
\pgfplotsset{compat=1.12}


\usepackage{ifthen}
% \usepackage[caption=false]{subfig}
% \usepackage{subfigure}
\usepackage[labelformat=simple]{subcaption}
\usepackage{float}
\usepackage{cleveref}
\crefname{equation}{Equation}{Equations}
\Crefname{equation}{Equation}{Equations}
\crefname{section}{Section}{Sections}
\Crefname{section}{Section}{Sections}
\crefname{figure}{Fig.}{Figures}
\Crefname{figure}{Fig.}{Figures}
\crefname{table}{Table}{Tables}
\Crefname{table}{Table}{Tables}
\crefname{algorithm}{Algorithm}{Algorithms}
\Crefname{algorithm}{Algorithm}{Algorithms}
\crefname{prop}{Proposition}{Propositions}
\Crefname{prop}{Proposition}{Propositions}
\crefname{algorithm}{Algorithm}{Algorithms}
\Crefname{algorithm}{Algorithm}{Algorithms}

\usepackage{keycommand}
\usepackage{enumerate}
\usepackage{geometry}
\usepackage{algorithm}
\usepackage{algorithmic}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
\renewcommand{\algorithmicforall}{\textbf{for each}}
\newcommand{\algorithmiccontinue}{\textbf{continue for}}
\newcommand{\CONTINUE}{\STATE \algorithmiccontinue}

% The follow codes are macros for frequent terms.
\newcommand{\etal}{\emph{et al}.}
\newcommand{\risk}{\mathscr{R}}
\newcommand{\fprotect}{\bm{f}_{\rm protected}}
\newcommand{\ffailed}{\bm{f}_{\rm failed}}
\newcommand{\acceptablerisk}{\risk_{\rm acceptable}}
\newcommand{\YES}{$\surd$}
\newcommand{\NO}{$\times$}

% If don't want to use the custom style, use the command \UseCustomStylefalse.
\newif\ifUseCustomStyle
\UseCustomStyletrue

% If don't want to use the custom style, use the command \UseCustomStylefalse.
\newif\ifUseCustomStyle
\UseCustomStyletrue

\ifUseCustomStyle
    \linespread{1.66}

    \newgeometry{left=1in,right=1in,top=1in,bottom=1in}
    \usepackage{caption}
    \captionsetup[figure]{
        font = footnotesize,
        labelsep = period,
        textformat = period,
        skip = 0pt
    }

    \captionsetup[table]{
        font = {footnotesize, sc},
        labelsep = newline,
        skip = 0pt
    }

    \extrarowsep = 0pt
    \tabulinesep = 1pt
\fi

% The following codes are used to draw help lines for tikzpicture.
\pgfmathdeclarefunction{gauss}{2}{%
  \pgfmathparse{1/(#2*sqrt(2*pi))*exp(-((x-#1)^2)/(2*#2^2))}%
}

\makeatletter
\def\grd@save@target#1{%
  \def\grd@target{#1}}
\def\grd@save@start#1{%
  \def\grd@start{#1}}
\tikzset{
  grid with coordinates/.style={
    to path={%
      \pgfextra{%
        \edef\grd@@target{(\tikztotarget)}%
        \tikz@scan@one@point\grd@save@target\grd@@target\relax
        \edef\grd@@start{(\tikztostart)}%
        \tikz@scan@one@point\grd@save@start\grd@@start\relax
        \draw[minor help lines] (\tikztostart) grid (\tikztotarget);
        \draw[major help lines] (\tikztostart) grid (\tikztotarget);
        \grd@start
        \pgfmathsetmacro{\grd@xa}{\the\pgf@x/1cm}
        \pgfmathsetmacro{\grd@ya}{\the\pgf@y/1cm}
        \grd@target
        \pgfmathsetmacro{\grd@xb}{\the\pgf@x/1cm}
        \pgfmathsetmacro{\grd@yb}{\the\pgf@y/1cm}
        \pgfmathsetmacro{\grd@xc}{\grd@xa + \pgfkeysvalueof{/tikz/grid with coordinates/major step}}
        \pgfmathsetmacro{\grd@yc}{\grd@ya + \pgfkeysvalueof{/tikz/grid with coordinates/major step}}
        \foreach \x in {\grd@xa,\grd@xc,...,\grd@xb}
        \node[anchor=north] at (\x,\grd@ya) {\pgfmathprintnumber{\x}};
        \foreach \y in {\grd@ya,\grd@yc,...,\grd@yb}
        \node[anchor=east] at (\grd@xa,\y) {\pgfmathprintnumber{\y}};
      }
    }
  },
  minor help lines/.style={
    help lines,
    step=\pgfkeysvalueof{/tikz/grid with coordinates/minor step}
  },
  major help lines/.style={
    help lines,
    line width=\pgfkeysvalueof{/tikz/grid with coordinates/major line width},
    step=\pgfkeysvalueof{/tikz/grid with coordinates/major step}
  },
  grid with coordinates/.cd,
  minor step/.initial=.2,
  major step/.initial=1,
  major line width/.initial=2pt,
}
\makeatother

% The following codes are used for TikZ/PGF
\tikzset{circuit declare symbol = ac source}
\tikzset{font = \fontsize{10pt}{12pt}\selectfont}
\tikzset{set ac source graphic = ac source IEC graphic}

\def\shadowtext[#1]#2at#3(#4)#5{
    % \pgfkeysalso{/tikz/.cd,#1}%
    \foreach \angle in {0,5,...,359}{
      \node[#1, text=white] at ([shift={(\angle:.8pt)}] #4){#5};
    }
    \node[#1] at (#4){#5};
}

\newcommand{\AddEvidence}[2]{
\draw[->] (#1,8) node [anchor = south] {#2} -- (#1,6.2);
}

\newcommand{\TimeScope}[3]{ % x , y , L
  \draw (#1,#2) -- (#1,#2 - 0.4);
  \draw (#1+#3,#2) -- (#1+#3,#2 - 0.4);
  \draw[<->] (#1,#2 - 0.2) -- (#1 + 0.5*#3,#2 - 0.2) node[fill = white] {$T$} -- (#1 + #3,#2 - 0.2);
}

\newcommand{\ClearEvidence}[1]{
  \draw [dashed, red] (#1 , 8) -- (#1, 6.2);
  \draw [dashed, red, ->] (#1, 3.4) -- (#1, 5.8);
  \draw [red] (#1, 3.4 - 0.15) node [anchor = south east] {Clear $\bm{E}_a$};
}

\newcommand{\computer}{\includegraphics[width=1cm]{./Figures/Materials/Computer.pdf}}
\newcommand{\router}{\includegraphics[width=1cm]{./Figures/Materials/Computer.pdf}}
\newcommand{\server}{\includegraphics[width=1cm]{./Figures/Materials/Server.pdf}}
\newcommand{\plc}{\includegraphics[width=0.95cm]{Figures/Materials/PLC.pdf}}

\newcommand{\bus}[3]{
  \draw[fill = white] (#1, #2 + 0.15) to[out = 180, in = 180] (#1, #2 - 0.15) -- (#1 + #3, #2 - 0.15) to[in = 0, out = 0] (#1 + #3, #2 + 0.15) -- cycle;
  \draw (#1 + #3, #2 + 0.1) to[out = 180, in = 180] (#1 + #3, #2 - 0.1) to[in = 0, out = 0] cycle;
}

\newcommand{\valve}[3]{
  \draw[fill = white] (#1,#2) -- (#1 - 0.25, #2 + 0.15) -- (#1 - 0.25, #2 - 0.15) -- (#1 + 0.25, #2 + 0.15) -- (#1 + 0.25, #2 - 0.15) -- (#1 ,#2) -- (#1, #2 + 0.2) -- (#1 - 0.2, #2 + 0.2) to[out = 45, in = 135] (#1 + 0.2, #2 + 0.2) -- (#1, #2 + 0.2);
  \node (v#3) at (#1,#2) [rectangle, minimum width = 0.5cm] {};
}

\newcommand{\BayesianNetwork}{
  \resizebox{!}{4.5cm}{
    \begin{tikzpicture}[line width = 1pt,
                        y = 0.6cm,
                        attack/.style =   {draw = red,       text = red,       fill = red,       circle, minimum size = 0.5cm, font = \tiny, inner sep=0pt},
                        resource/.style = {draw = darkgreen, text = darkgreen, fill = darkgreen, circle, minimum size = 0.5cm, font = \tiny, inner sep=0pt},
                        function/.style = {draw = blue,      text = blue,      fill = blue,      circle, minimum size = 0.5cm, font = \tiny, inner sep=0pt},
                        incident/.style = {draw = black,     text = black,     fill = black,     circle, minimum size = 0.5cm, font = \tiny, inner sep=0pt}]
      \input{./Figures/BayesianNetworkCode}
    \end{tikzpicture}
  }
}

\newcommand{\EvidenceList}{
  \resizebox{3.5cm}{!}{
    \extrarowsep = 8pt
    \everyrow{\tabucline[1.5pt white]{-}}
    \begin{tikzpicture}[line width = 1pt]
      \taburowcolors[2] 8{black!10 .. black!30} 
      \node at (0,0) {
      \begin{tabu} to 9.4cm{*2{X[c,-1]|[1.5pt white]}X[l]|[1.5pt white]X[c,-1]} 
        \rowcolor{black!80}\rowfont\bfseries 
        \textcolor{white}{Start} & 
        \textcolor{white}{End} & 
        \textcolor{white}{Description} & 
        \textcolor{white}{Symbol}\\
        \input{./Tables/EvidenceList}
      \end{tabu}
      };
    \end{tikzpicture}
  }
}

\newcommand{\myscope}[2]{
  \draw (#1, #2 - 0.7) rectangle (#1 + 1, #2 + 0.7);
  \draw (#1 + 0.1, #2 + 0.1) rectangle (#1 + 0.9, #2 + 0.6);
  \node at (#1 + 0.5, #2 + 0.9) {\scriptsize Scope};
}

\definecolor{darkgreen}{rgb}{0, 0.3, 0}
\definecolor{lightblue}{rgb}{0.7, 0.7, 1}

\newcommand{\mydot}{,}
\newcommand{\ES}{ES}
\newcommand{\HDS}{HDS}
\newcommand{\es}{ES}
\newcommand{\hds}{HDS}

\newcommand{\vd}{\raisebox{0pt}[0pt][0pt]{$\vdots$}}
\newcommand{\TT}{\texttt{T}}
\newcommand{\FF}{\texttt{F}}
\newcommand{\TTT}{\cellcolor{lightblue}\texttt{T}}
\newcommand{\FFF}{\cellcolor{lightblue}\texttt{F}}
\newcommand{\added}[1]{\textcolor[rgb]{1.00,0.00,0.00}{#1}}
