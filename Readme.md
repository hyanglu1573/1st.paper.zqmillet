## Notice
- This paper is submitted to IEEE Transaction on Systems, Man and Cybernetics: Systems.
- This paper is under review.
- I've added the `Main.pdf` into the file `.gitignore`. So, if you want to obtain the `.pdf` file of the full text, pull the whole project and compile it with `XeLaTeX`, please.
- To manage references, the reference management software **Jabref** is suggested. The format of BibTeX key is `[auth]-[year]-p[firstpage]-[lastpage]`.