%!TEX root = ../Main.tex

\section{Simulation: Chemical Reactor Control System}
\label{sec:Simulation: Chemical Reactor Control System}
The purpose of this section is to illustrate how our approach calculates the cybersecurity risk validly in real time through a realistic simulation. In this section, we start by describing the experimental subject -- a chemical reactor control system. Then we describe the model implementation and introduce the simulation platform. We design several simulations to illustrate the real time, validity and ability to handle unknown attack of our approach. At last, this section gives and analyzes results of simulations.

\subsection{Knowledge Modelling and Simulation Platform}
\label{sec:Knowledge Modelling and Simulation Platform}
Chemical reactor is a device for containing and controlling a chemical reaction, it is widely used in chemical industry. The representative structure of chemical reactor control system is given in \cref{fig:Control Structure of Chemical Reactor}.
\begin{figure}[!ht]
  \centering
  \input{./Figures/ControlStructureOfChemicalReactor}
  \caption{Control Structure of Chemical Reactor}
  \label{fig:Control Structure of Chemical Reactor}
\end{figure}

In \cref{fig:Control Structure of Chemical Reactor}, ethernet connects with the enterprise network which is not shown in this figure via G1. Two CANBUS networks connect to the Ethernet via G2 and G3. In the Ethernet, there are an engineer station (ES) and a historical data server (HDS). The host in enterprise network can access the historical data of HDS, but cannot access the ES. PLC1-PLC6 are distributed in two CANBUS networks. ES and HDS can obtain data of all PLCs, only ES can modify and configure PLCs.

The control system has intentionally been set up to include several real vulnerabilities. In particular, the historical data server is vulnerable to a buffer overflow exploitation based on CVE-2007-4060 and a FTP bounce attack based on CVE-1999-0017. Additional, the historical data server does not limit the number of username/password verifications, which makes the historical data server vulnerable to password brute-force attacks. Like the historical data server, the engineer station is also vulnerable to a buffer overflow exploitation. And, more remarkable, the engineer station relies on the IP address for authentication, which allows remote attackers to send malicious code by spoofing the IP address. When attacker obtains the administrator authorities of historical data server or engineer station, he can attack PLCs by DoS attack, man-in-the-middle attack, etc.

If attacker launches attack on PLC1-6, the corresponding functions will be failed. For instance, when PLC1 is under the DoS attack, the switch functions of V1 and V2 will be invalid. Similarly, if attacker reconfigure the program of PLC2, the sensation function will be failed. As the subfunction of liquid level control, the failure of switch function of V1 is likely to lead to invalidation of liquid level control. What is worse, the invalidation of function may cause unanticipated incidents, such as temperature anomaly, excessive pressure, heater dry fired and even reactor explosion. Finally, the series of incidents will damage productions and facilities, pollute water and air, and injure staffs. We enumerate all potential attacks, figure out functions which may be caused by those attacks, then speculate all possible incidents, at last, build the multi-level Bayesian network which is given by \cref{fig:Multi-level Bayesian Network of Reactor}. Condition probabilities of the nodes in multi-level Bayesian network are obtained by expertise.

\begin{figure}[!ht]
  \centering
  \resizebox{\textwidth}{!}{
    \input{./Figures/MultiLevelBayesianNetworkOfReactor}
  }
  \caption{Multi-level Bayesian Network of Reactor}
  \label{fig:Multi-level Bayesian Network of Reactor}
\end{figure}

The simulation platform is implemented in MATLAB. The simulation platform is consist of three modules: evidence generator, incident prediction module and risk assessment module. \Cref{fig:Structure of Simulation Platform} gives the structure of the simulation platform.
\begin{figure}[!ht]
  \centering
  \resizebox{\textwidth}{!}{
    \input{./Figures/StructureOfSimulationPlatform}
  }
  \caption{Structure of Simulation Platform}
  \label{fig:Structure of Simulation Platform}
\end{figure}

The evidence generator is employed to replace the real signature-based intrusion detection system and anomaly detection system. It has an evidence list which will be shown in detail in \cref{sec:Simulation and Result Analysis}, and every evidence in this list has a time parameter. The input of evidence generator is a time trigger and its period is $1$ minute. When evidence generator receives the trigger signal, it reads the current time and finds the evidence of which time parameter equals to current time. If the evidence exists, it will be sent to multi-level Bayesian network. The evidence generator can realize the function of signature-based intrusion detection system and anomaly detection system.

The incident prediction module uses the Bayes net toolbox (BNT) to establish multi-level Bayesian network which is shown in \cref{fig:Multi-level Bayesian Network of Reactor}. BNT developed by Kevin Murphy is a toolbox that works with Matlab from MathWorks. The toolbox supports different exact and approximate inference algorithms, parameter and structure learning. When evidences are sent by evidence generator, they will be added into $\bm{E}$. Then incident prediction module uses the BNT to infer the multi-level Bayesian network with $\bm{E}$. At last, probabilities of $x_1, x_2 \cdots, x_8$ are given and sent to risk assessment module.

When Risk assessment module receives the probabilities of $x_1, x_2 \cdots, x_8$, it calculates the risk of every incidents, then adds all potential losses of $x_1, x_2 \cdots, x_8$ into system risk.

\Cref{fig:Interface of Simulation} shows the interface of the simulation which consists of two windows. The left window displays the multi-level Bayesian network. We use four color -- red, green, blue and black -- to represent four kinds of nodes -- attack nodes, resource nodes, function nodes and incident/auxiliary nodes. When incident prediction module receives attack evidence or anomaly evidence, the corresponding node will be marked with circle. Double clicking any node can open its properties window. In Figure 9, the properties window of incident node $x_7$ shows the current probability of $x_7$ in parameter \texttt{UserData}. The right window shows the probability curves of $x_1,x_2,\cdots,x_8$ and dynamic cybersecurity risk curve. Every minute, in the right window, points are plotted at above curves according to results sent from incident prediction module and risk assessment module. In \cref{fig:Interface of Simulation}, the right window shows probabilities of $x_1,x_2,\cdots,x_8$ and the risk in the first 345 minutes.
\begin{figure}[!ht]
  \centering
  \includegraphics[width=\textwidth]{./Figures/SimulationInterface.png}
  \caption{Interface of Simulation}
  \label{fig:Interface of Simulation}
\end{figure}

\subsection{Simulation and Result Analysis}
\label{sec:Simulation and Result Analysis}
The simulation procedure is separated into three steps as follows. (1) We launch a multi-step attack which will be described later on chemical reactor control system, collect evidences and calculate cybersecurity risk every minute. Then we will give the curves of cybersecurity risk and probabilities of incidents $x_1, x_2 \cdots, x_8$ in multi-level Bayesian network. (2) To validate the ability to deal with the unknown attacks, we remove several attack knowledge from multi-level Bayesian network, so these attacks will be unknown attacks for system, then launch the identical multi-step attack on the system. At last, compare the results of these two simulations. (3) We record the execution time of 5000 times calculation, then give the distribution curve of execution time of our approach to show the real-time of our approach.

Because what we concern about is cybersecurity of physical layer, to simplify the process of attack, we assume that the attack has reached the physical layer. Attacker’s goal is to destroy the chemical reactor by invalidating PLC5, and the attack scenario included the following steps. Firstly, the remote attacker gets the list of IP addresses by ip-sweep. Secondly, attacker scans ports and vulnerabilities of historical data server and engineer station. Thirdly, attacker launches DoS attack to historical data server and makes it breakdown. Fourthly, attacker disguises as historical data server to communicate with engineer station. Since engineer station has confidence in historical data server, the data and command sent by attacker won’t be confirmed, as a result, attacker can send malicious command to engineer station and get the administrator authority. Finally, attacker modifies the program of PLC5 to invalidate the pressure reducing function.

Since aforementioned attacks are all known attacks, they will be detected by signature-based intrusion detection system. On the other hand, some attacks will cause system anomalies, and will be captured by anomaly detection system. For instance, the attacker launches IP sweep attack, at the same time, the intrusion detection system detects this attack and generates an attack evidence. Similarly, when pressure control function is failed, the anomaly detection system will generate an anomaly evidence. For clarity, \cref{tab:List of Evidences} lists all evidences caused by this multi-step attack. In this simulation, we use the positive integer to represent absolute time, and the unit is minute.
\begin{table}[!ht]
  \centering
  \caption{List of Evidences}
  \label{tab:List of Evidences}
  \begin{tabu} to 0.7\textwidth{*2{X[c,-1]|}X[l]|X[c,-1]}
    \tabucline[1.5pt]{-}
    Start Time & End Time & Description & Symbol\\
    \tabucline[1.5pt]{-}
    \everyrow{\hline} 
    \input{Tables/EvidenceList}
    \tabucline[1.1pt]{-}
  \end{tabu}
\end{table}

In this simulation, the maximum interval of adjacent continuous atom attacks is set to 150 minutes. There are eight incidents which can lead to various losses. Consequences of these eight incidents are quantified and given in \cref{tab:Quantification of Consequences of Incidences}.
\begin{table}[!ht]
    \centering
    \caption{Quantification of Incidences Consequences}
    \label{tab:Quantification of Consequences of Incidences}
    \begin{tabu}to 0.7\textwidth{X[c,-1]|X[-1]|X[-1]}
    \tabucline[1.5pt]{-}
        Incident Symbol   & Description of Incident   & Quantification of Consequence(\$)\\
    \tabucline[1.5pt]{-}
        $x_{1}$  & Production damaged           & $50000$  \\\hline
        $x_{2}$  & Tank damaged                 & $500000$ \\\hline
        $x_{3}$  & Heater damaged               & $10000$  \\\hline
        $x_{4}$  & Sensors damaged              & $10000$  \\\hline
        $x_{5}$  & Staff$_{\text{1-4}}$ injured & $800000$ \\\hline
        $x_{6}$  & Staff$_{\text{5-9}}$ injured & $1000000$\\\hline
        $x_{7}$  & Water pollution              & $200000$ \\\hline
        $x_{8}$  & Air pollution                & $200000$ \\
    \tabucline[1.5pt]{-}
    \end{tabu}
\end{table}

\cref{fig:The Results of Simulation} shows the probabilities of incidents $x_1, x_2 \cdots, x_8$ and the value of dynamic cybersecurity risk which we record every minute. In \cref{fig:The Results of Simulation}, the label with a pin on the risk curve represents the corresponding evidence. For example, $a_1$ means that at 50th minute, signature-based intrusion detection system detected the IP sweep attack, $f_4$ means that at 266th minute, the anomaly detection system captured the failure of $f_4$, $\overline{f}_4$ means that the function $f_4$ has been fixed at 378th minute. The last label ``Attack timeout'' at 412th minute means that it has passed 150 minutes from the last attack evidence $a_{20}$ at 261st minute.
\begin{figure}[!ht]
  \begin{flushright}
    \input{./Figures/CurvesOfIncidentProbabilities}\\
    \input{./Figures/CurveOfCybersecurityRisk}
  \end{flushright}\centering  
  \caption{The Results of Simulation}
  \label{fig:The Results of Simulation}
\end{figure}

\cref{fig:The Results of Simulation} shows that the cybersecurity risk is increasing as attacker launches attacks step by step. On the other hand, when attack is suspended or invalid function is fixed, the cybersecurity risk decreases. It’s worth noting that the damage probability of production is larger than that of tank before $e_4$ occurs. One of the main reasons is that it is incapable of inferring the purpose of attacker until $e_4$ occurs. Another primary reason is that the causes of production damage are more than that of tank damage. When incident $e_4$ occurs and is captured, the attack target is evident. Thus after 310th minute the damage probability of tank is larger than that of production. In \cref{fig:The Results of Simulation} we can see that the recovery of $f_4$ or $f_{12}$ does not reduce the cybersecurity risk, because that the pressure is still excessive. The cybersecurity is decreasing as the pressure is reduced below the safe threshold.

To illustrate the ability to deal with unknown attack, we remove attack nodes $a_6$ and $a_9$ attack knowledge from multi-level Bayesian network. The incident prediction module do not know that an attacker can get administrator authority of engineer station by DoS attack and IP spoofing attack. In other words, $a_6$ and $a_9$ are unknown attacks for incident prediction module. Additional, the condition probability table of resource node $r_9$ also need to modify. \cref{tab:Modification of Condition Probability} gives the condition probability table of resource node $r_9$ before the modification. By removing 3nd row and 6th to 9th columns which are marked with blue, we can obtain the modified condition probability table of resource node $r_9$.
\begin{table}[!ht]
    \centering
    \caption{Modification of Condition Probability}
    \label{tab:Modification of Condition Probability}
    \begin{tabu}to 0.7\textwidth{X|*8{X[c]}}

        $L(a_7)$ & \FF  & \TT  & \FF  & \TT  & \FFF  & \TTT  & \FFF  & \TTT  \\
        $L(a_8)$ & \FF  & \FF  & \TT  & \TT  & \FFF  & \FFF  & \TTT  & \TTT  \\
        \cellcolor{lightblue}$L(a_9)$ & \FFF & \FFF & \FFF & \FFF & \TTT  & \TTT  & \TTT  & \TTT  \\
        \hline
        $O(r_9)$ & $c_{r_9,1}$ & $c_{r_9,2}$ & $c_{r_9,3}$ & $c_{r_9,4}$ & \cellcolor{lightblue}$c_{r_9,5}$ & \cellcolor{lightblue}$c_{r_9,6}$ & \cellcolor{lightblue}$c_{r_9,7}$ & \cellcolor{lightblue}$c_{r_9,8}$ \\
    \end{tabu}
\end{table}

We launch the same multi-step attack on chemical reactor control system. Since there is no knowledge of attacks $a_6$ and $a_9$, we need to remove evidences of $a_6$ and $a_9$ from evidence list which is given in \cref{tab:List of Evidences}. We record cybersecurity risk every minute, then we put risk curves of two simulations in one figure which is shown in \cref{fig:Comparison of Risk Curves of Two Simulation}.
\begin{figure}[!ht]
  \centering
  \input{./Figures/ComparisonOfRiskCurvesOfTwoSimulation}
  \caption{Comparison of Risk Curves of Two Simulation}
  \label{fig:Comparison of Risk Curves of Two Simulation}
\end{figure}

\Cref{fig:Comparison of Risk Curves of Two Simulation} shows that before 120th minute the risk of second simulation is slightly below that of first simulation. The reason is that without knowledge of $a_6$ and $a_9$ the probability of attack obtaining the resource $r_9$ is lower in the view of incident prediction module. After 120th minute and before 259th minute, there is difference between two risk curves. Since there is no evidences of $a_6$ and $a_9$, the risk of second simulation in this range remains unchanged. After 259th minute, we can see risk curves of two simulations are overlapped. This comparison shows that without knowledge of some atom attacks, there is no comparatively large deviation in the result of risk assessment. In the nutshell, if there are a few of unknown atom attacks in multi-step attack, our approach can still generate a relatively accurate risk value.

At last, we design a stochastic evidence generator for testing execution time of our dynamic risk assessment approach. This stochastic evidence generator can generate attack and anomaly evidences randomly every minute. The proportion of evidence is 10\%, in other words, the stochastic evidence generator sends average 1 evidence to risk assessment module every 10 minutes. We use the stochastic evidence generator to replace the evidence generator in first two simulations, then record the execution times of 5000 calculations. This simulation is run on a machine with Intel Pentium processor G3220(3M Cache, 3.00GHz) and 4GB DDR3 memory. \Cref{fig:Distribution of Execution Time} shows the distribution of 5000 execution times.
\begin{figure}[!ht]
  \centering
  \input{Figures/DistributionOfExecutionTime}
  \caption{Distribution of Execution Time}
  \label{fig:Distribution of Execution Time}
\end{figure}

The average execution time of risk assessment is 0.0941s, the maximum execution time of risk assessment is 0.1316s. The performance of our approach can satisfy the real-time requirement of dynamic risk assessment.