classdef (Sealed) Constant   
    properties (Constant)
        MinimumCalculationError = 1e-10;
    end
end

