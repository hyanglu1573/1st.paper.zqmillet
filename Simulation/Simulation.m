SimScripts.CreateNodes;
SimScripts.SetConditionalProbabilityTables;
SimScripts.CreateBayesianNetwork;

TestType = 'Known';
Functions.SaveVariable('./', TestType);
SimScripts.TestRiskAssessment;

TestType = 'Unknown';
Functions.SaveVariable('./', TestType);
SimScripts.TestUnknownAttack;

% SimScripts.TestExecutionTime;
SimScripts.ProcessData;


