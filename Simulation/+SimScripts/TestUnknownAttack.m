r09.AddAllParents(a08, a07, ...
    0.00000001, ... F    F
    0.80000000, ... F    T
    0.10000000, ... T    F
    0.90000000);  % T    T

Nodes = BayesianNetwork.Nodes;

DeleteNodes = {a06, r08, a09};
DeleteNodeIndex = zeros(size(DeleteNodes));
Index = 1;
for i = 1:numel(DeleteNodes)
    for j = 1:numel(Nodes)
        if isequal(Nodes{j}, DeleteNodes{i})
            DeleteNodeIndex(1, Index) = j;
            Index = Index + 1;
            break;
        end
    end
end
Nodes(DeleteNodeIndex) = [];

BayesianNetwork = Classes.BayesianNetwork();
BayesianNetwork.AddNodes(Nodes{:});
BayesianNetwork.Initialize();

SimScripts.TestRiskAssessment;