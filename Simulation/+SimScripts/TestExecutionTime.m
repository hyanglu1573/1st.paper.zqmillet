EvidenceProportion = 0.1;
SimulationTime = 2000;

for i = 1:SimulationTime
    BayesianNetwork.RemoveEvidences(BayesianNetwork.Nodes{:});
    for j = 1:numel(EvidenceList)
        if rand() < EvidenceProportion
            BayesianNetwork.AddEvidences(BayesianNetwork.Nodes{j});
        end
    end
    
    tic;
    BayesianNetwork.Inference();
    ExecutionTime = toc;
    FileExecutionTime = fopen('./OutputData/ExecutionTime.dat', 'a+');
    fprintf(FileExecutionTime, '%d\n', ExecutionTime);
    fclose(FileExecutionTime);

    disp(['Simulating ', num2str(i), '/', num2str(SimulationTime), ', ', ...
        'the execution time is ', num2str(ExecutionTime), 's']);
end
